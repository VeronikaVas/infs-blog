<?php
include_once 'admin/classes/database.php';

class User{
	private $db;

	function __construct() {
		$this->db = new Database();
	}
	
	function getAuthor($user_id){
		$sql = "SELECT username FROM user WHERE id_user=$user_id ";
		$result = $this->db->select($sql);
		
		$row = $result->fetch_assoc();
		$return = $row['username'];
	
		return $return;
	}
	
}
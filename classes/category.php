<?php

include_once 'admin/classes/database.php';

class Category {

	private $db;

	function __construct() {
		$this->db = new Database();
	}
	
	
	public function getUrl($page = NULL, $id) {
		if ($page == NULL) {
			return '/?path=category.php&id=' . $id;
		} else {
			return '/?path=articles&action=category&id=' . $id ;
		}
	
	}

	
	function getCategoryById($id){
		$sql = "SELECT * FROM category WHERE id=$id";
		$result = $this->db->select($sql);

		if($result->num_rows > 0){	
			return $result->fetch_assoc();
		}
	}
	
	public static function getCategory($id){
		$sql = "SELECT name FROM category WHERE id=$id";
		$result = $db->select($sql);
	
		if($result->num_rows > 0){
			return $result->fetch_assoc();;
		}
	}
	
	private function buildCategoryTree(&$output, $id = 0)
	{
		if ($id == 0) {
			$req = $this->db->select("SELECT * FROM category WHERE id_parent=0");
		} else {
			$req = $this->db->select("SELECT * FROM category WHERE id_parent = $id");			
		}

		if ($req->num_rows == 0) {
			$output = [];
		} else {
		while($category = $req->fetch_assoc()) {
				$output[$category['id']]['category'] = $this->getCategoryById($category['id']);
				$output[$category['id']]['childs'] = [];
				
				$this->buildCategoryTree($output[$category['id']]['childs'], $category['id']);
			}
		}
	}
	

	public function all()
	{
		$categories = [];
		$this->buildCategoryTree($categories);
	
		return $categories;

	}
}
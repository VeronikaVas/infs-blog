<?php
include_once 'admin/classes/database.php';

class Article {

	private $db;

	function __construct() {
		$this->db = new Database();
	}
	
	public function getPopularityArticles($count){
		$return = array();
		$query = "SELECT * FROM post LIMIT $count ";
		$result = $this->db->select($query);

		if($result->num_rows > 0){
			while($row = $result->fetch_assoc()) {
				array_push($return, $row);
			}
		}
		
		return $return;
	}
	
	public function month_to_word($str)
	{
		$dates = Array(
				"1" => "January",
				"2" => "February",
				"3" => "March",
				"4" => "April",
				"5" => "May",
				"6" => "June",
				"7" => "July",
				"8" => "August",
				"9" => "September",
				"10" => "October",
				"11" => "November",
				"12" => "December",
				);
		$month=$dates[$str];
		return $month;
	}

	public function getArticle($id){
		$query = "SELECT * FROM post WHERE id_post = $id";
		$result = $this->db->select($query);
		if($result->num_rows > 0){
			while($row = $result->fetch_assoc()) {
				return $row;
			}
		}
	}


}
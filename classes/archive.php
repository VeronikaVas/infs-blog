<?php
include_once 'admin/classes/database.php';

class Archive{
	private $db;

	function __construct() {
		$this->db = new Database();
	}
	
	public function getArchives(){	
		$query = "SELECT MONTH(date) as month, YEAR(date) as year, count(*) as count FROM post GROUP BY MONTH(date) ORDER BY date DESC";
		$result = $this->db->select($query);
        $return = array();
        
        while ($row = $result->fetch_assoc()){
            array_push($return, $row);
        }

        return $return;  
	}
	
	
	
	function getPostByMontYear($month, $year){
		$return = array();
		
		$query = "SELECT * FROM post WHERE MONTH(date) = $month AND YEAR(date) = $year ORDER BY date DESC";
		$result = $this->db->select($query);
	
		while ($row = $result->fetch_assoc()){
			array_push($return, $row);
		}
		return $return;
	}
	
	
	
}
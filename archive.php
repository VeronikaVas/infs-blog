<?php include_once './views/header.php'; 
include_once 'views/template-functions.php';
include_once 'classes/archive.php';?>
<section class="archives">
    <h2>archives</h2>
    <?php
                if (isset ($_GET["month"]) && isset ($_GET["year"])){              	
                	printMonthArchive($_GET["month"],$_GET["year"] );
                	
                } else {
                	printArchive();
                }
            ?>
    
</section>

<?php include_once 'views/aside-panel.php'; ?>	
<div class="clearfix"></div>
<?php include_once './views/footer-block.php';?>
<?php include_once './views/footer.php';?>

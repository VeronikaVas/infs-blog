<?php
    session_start();
?>

<!doctype html>
<head>    
  <title>Zend
  </title>    
  <meta charset="utf-8">    
  <link rel="stylesheet" type="text/css" href="../style/style.css">
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,900' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Passion+One:400,900,700' rel='stylesheet' type='text/css'>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
</head>
<body>         
  <div id="up"></div>    
  <div id="container">
  	<div id="top-menu">         
      <ul>        
          <li><a href="gallery.php">galerry</a></li>       
          <li>/</li>    
          <li><a href="about.php">about</a></li>      
          <li>/</li>        
          <li> <a href="contact.php">contact</a></li>
          <li>/</li>        
          <li> <a href="./admin/login.php">admin</a></li>   
      </ul>
      
      <form id="search" action="search.php" autocomplete="on" method="GET">
        <input name="search" required="required" type="search" id="search-input" value="" placeholder="Type Keyword and hit enter"/>
        <input  type="submit" class="search-submit" value="go" />
        <div id="search-results">
        </div>
      </form>
  
      <div class="clearfix">
      </div>      
	</div>
  	<header>
  		<div id="logo">
        	<h1><a href="index.php">Zend</a></h1>
        	<h2><a href="index.php">A stylish blog/magazine theme</a></h2>
       </div>
      <div id="social-media">
        <a href="#" class="popup"><img src="images/design/social-media/twitter_icon.jpg" alt="Twitter"></a>
        <a href="#" class="popup"><img src="images/design//social-media/facebook_icon.jpg" alt="Facebook"></a>
        <a href="#" class="popup"><img src="images/design/social-media/pinterest_icon.jpg" alt="Pinterest"></a>
        <a href="#" class="popup"><img src="images/design/social-media/linkedin_icon.jpg" alt="Linkedin"></a>
        <a href="#" class="popup"><img src="images/design/social-media/rss_icon.jpg" alt="RSS"></a>
        <script>
        jQuery(document).ready(function($){
            $('.popup').click( function(e) { 
            	swal("I am pop up window!");
            });
         
         });
        </script>
	  </div>

	  <div class="clearfix"></div>
  	  <nav id="main-menu">
         <ul>
         	<li><a href="index.php">home</a></li>
        	<?php 
        	$categories = new Category();
        	printCategories($categories->all());?>
        </ul>
	 </nav>
  	</header>
    
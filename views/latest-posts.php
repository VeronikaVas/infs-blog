<div id="latest-posts-block">
         <div class="section-block-header">
             <h2>latest posts</h2>
             <ul>
                <li><a href="#" id="right-arrow"> &gt; </a></li>
                <li><a href="#" id="left-arrow"> &lt; </a></li>
            </ul>
            <div class="clearfix"></div>
         </div>
         
         <div class="latest-post-left">
            <img src="pictures/body1.jpg" alt="article-icon">
            <span class="date">May 26</span>  <span class="by-article"> by </span> <span class="author">John Pixle</span><br>
            <h2>robot love and the business of free</h2>
            <p>Lorem ipsum dolor sit consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, ...</p>
            <a href=#>Read More</a>
         </div>
         
         <div class="latest-post-right">
            <img src="pictures/body2.jpg" alt="article-icon">
            <span class="date">May 26</span>  <span class="by-article"> by </span> <span class="author">John Pixle</span><br>
            <h2>robot love and the business of free</h2>
            <p>Lorem ipsum dolor sit consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, ...</p>
            <a href=#>Read More</a>
         </div>
         
         <div class="latest-post-left">
            <img src="pictures/body1.jpg" alt="article-icon">
            <span class="date">May 26</span>  <span class="by-article"> by </span> <span class="author">John Pixle</span><br>
            <h2>robot love and the business of free</h2>
            <p>Lorem ipsum dolor sit consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, ...</p>
            <a href=#>Read More</a>
         </div>
         
         <div class="latest-post-right">
            <img src="pictures/body2.jpg" alt="article-icon">
            <span class="date">May 26</span>  <span class="by-article"> by </span> <span class="author">John Pixle</span><br>
            <h2>robot love and the business of free</h2>
            <p>Lorem ipsum dolor sit consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, ...</p>
            <a href=#>Read More</a>
         </div>
         
</div>
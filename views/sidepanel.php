<aside>
       <!-- popular posts block -->
        <div class="aside-block">
           <h2>popular posts</h2>
           <?php printPopularArticlesSidebar(); ?>
           
        </div>
        <div class="aside-block">
           <img src="pictures/adv.jpg" alt="ad-ad" width="261" height="121">
        </div>
        
        <div class="aside-block">
           <h2>latest tweets</h2>
           <div class="aside-tweet">
             <span class="tweet-author">@JohnPixle</span>
               Let us know if we can help buddy! <br>
             <span class="tweet-time">10 minutes ago</span>
           </div>
           
           <div class="aside-tweet">
             New Article: RobotLove. Stay tuned. <br>
            <span class="tweet-time">14 minutes ago</span>
           </div>
           
           <div class="aside-tweet">
              We would like to thank all of our readers for the awesome feedback we recieved.<br>
              <span class="tweet-time">10 minutes ago</span>
            </div>
            
            <span id="twitter-follow-us">Follow Us</span>
        </div>
         
         <!-- featured video block -->
        <div class="aside-block">
          <h2>featured video</h2>
          <img src="pictures/aside-featured-video.jpg" alt="featured-video">
        </div>
        
        <!-- advertise block -->
        <div class="aside-block">
          <div id="double-adv">
            <img src="pictures/adv-small.jpg" alt="small-ad">
            <img src="pictures/adv-small.jpg" alt="small-ad">
            
          </div>
          <div class="clearfix"></div>
        </div>
        
        <div class="aside-block">
          <h2>popular tags</h2>
          <ul id="tag-list">
             <li><a href=#>Awesome</a></li>
             <li><a href=#>Tag</a></li>
             <li><a href=#>Cloud</a></li>
             <li><a href=#>For</a></li>
             <li><a href=#>Your</a></li>
             <li><a href=#>Most</a></li>
             <li><a href=#>Popular</a></li>
             <li><a href=#>Tags</a></li>
             <li><a href=#>Awesome</a></li>
             <li><a href=#>Tag</a></li>
             <li><a href=#>Cloud</a></li>
             <li><a href=#>For</a></li>
             <li><a href=#>Your</a></li>
             <li><a href=#>Most</a></li>
             <li><a href=#>Popular</a></li>
             <li><a href=#>Tags</a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        
        <!-- archive block -->
        <div class="aside-block">
            <h2><a href="archive.php">archives</a></h2>
            <?php printArchiveSidebar();?>
        </div> 
</aside>

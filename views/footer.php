<footer>
      <div class="footer-block">
         <h2>featured posts</h2>
         <?php printPopularArticlesFooter()?> 
      </div>
      
      <div class="footer-block">
          <h2>featured video</h2>
           <iframe width="260" height="160" src="https://www.youtube.com/embed/XJyqMdgt2ss" ></iframe>
      </div>
      
      <div class="footer-block">
         <h2>a text widget</h2>
         <p>Lorem ipsum dolor sit consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla consequat massa quis enim.</p>
      </div>
      
      
    
	</footer>
	<div class="clearfix"></div> 
	<nav id="footer-menu">
      <ul>
      	<?php  $categories = new Category(); printCategories($categories->all(), $only_first_level = true); ?>                   
      </ul>

	</nav>    
  </div>
</body>     
</html>

<?php

function printArticle($id){
	
	$post = new Article();
	$result = $post->getArticle($id);
	
	echo "<div class='post-post'>";
	$day = date_parse($result['date'])['day'];
	$month = month_to_word(date_parse($result['date'])['month']);
	
	$name = 'ver';
	echo "<span class='post-info'>".$month."".$day."</span> by <span class='post-info'>".$name."</span><br>";
	echo "<h2>".$result["header"]."</h2>";
	//echo "<img src=".strstr($post["picture_url"], "/").">";
	
	echo '<img src="images/slider.jpg" alt="Linkedin">';
	echo "</div>";
	echo html_entity_decode($result['text']);
	
	//get_post_author($result['id_user'])
}

function printCategories($categories, $only_first_level = false, $deep = 0)
{
    if ($deep == 1) {
        echo '<ul class="subitems">';
    }

    foreach ($categories as $category) {
        echo '<li>';
        $aaa = $category['category']['id'];
        echo '<a href="./?path=category.php&id='.$category['category']['id'].'">'.$category['category']['name'].'</a>';

        $subcategories = $category['childs'];

        if (count($subcategories) > 0 && !$only_first_level) {
            printCategories($subcategories, false, $deep + 1);
        }
        echo '</li>';
    }

    if ($deep == 1) {
        echo '</ul>';
    }
}

function printArchiveSidebar() {
    echo '<ul class="month-archive">';
   
    $archive = new Archive();
    $result = $archive->getArchives();
     
    foreach ($result as $row){
    	$month = month_to_word($row['month']);
    	$year = $row['year'];
    	echo "<li><a href='archive.php?month=".$row['month']."&year=".$year."'>".$month." ".$year."</a>&nbsp;(".$row['count'].")</li>";
    }
    echo '</ul>';         
}

function printMonthArchive($month, $year) {
	echo "<div class='archive-block'>";
	echo "<h3>".month_to_word($month)." ".$year."</h3>";
	
	$archive = new Archive();
	$category = new Category();
	$result = $archive->getPostByMontYear($month, $year);
	
	foreach ($result as $row){
		$day = date_parse($row['date'])['day'];
		
		echo "<div class='archive-post'>";
		echo "<h4><a href=./?path=post.php&id=".$row['id_post'].">".$row['header']."</a></h4>";
		echo "<span>".month_to_word($month)." ".$day."</span>";
		
		echo " in category ";
		$category_id = $row['id_category'];
		
		echo "<span><a href=category.php?id=".$category_id.">". $category->getCategoryWithId($category_id)."</a></span>";
		echo "</div>";
	}
	
	echo "</div>";

}

function printArchive() {
	echo "<div class='archive-block'>";
	
	$archive = new Archive();
	$category = new Category();
	$result = $archive->getArchives();

	foreach ($result as $row){
		$month = $row['month'];
		$year = $row['year'];
		
		printMonthArchive($month, $year);
	}

	echo "</div>";
}

function printPopularArticlesSidebar()
{
	$post = new Article();
	$user = new User();
	$count = 4;
	$result = $post->getPopularityArticles($count);
	
	foreach ($result as $row){
		echo "<div class='popular-post'>";
		echo "<img src='../images/7.jpg' alt='article-icon'>";
		
		$day= date_parse($row['date'])['day'];
		$year= date_parse($row['date'])['year'];
		$month = month_to_word(date_parse($row['date'])['month']);
		
		echo "<a href='./?path=post.php&id=".$row["id_post"]."'>";
		echo "<h3>".$row['header']."</h3></a>";
		echo '<span class="post-info">';
		echo $month." ".$day.", ".$year;
		echo '</span>';
		echo ' by ';
		echo '<span class="post-info">';
		echo $user->getAuthor($row['id_user']);
		echo '</span>';
		echo "</div>";
		echo "<div class=\"clearfix\"></div>";
	}

}

function printPopularArticlesFooter(){

	include_once './classes/user.php';
	include_once './classes/article.php';
	
	$user = new User();
	$post = new Article();
	$count = 2;
	$result = $post->getPopularityArticles($count);
	
	for ($i=0; $i<count($result);$i++){
		$header = $result[$i]["header"];
		 
		echo "<div class='footer-featured-block'>";
		echo "<img src='../images/7.jpg' alt='small-ad'>";
	
		echo "<a href='index.php?path=post.php&id=".$result[$i]["id_post"]."'><h3>".$header."</h3></a>";
		$day= date_parse($result[$i]['date'])['day'];
		$month = month_to_word(date_parse($result[$i]['date'])['month']);
		echo "<span class='footer-date'>".$month."".$day."</span> by <span class='footer-author'>".$user->getAuthor($result[$i]['id_user'])."</span>";
		echo "</div>";
		echo "<div class='clearfix'></div>";
	}
}

function month_to_word($str)
{
	$dates = Array(
			"1" => "January",
			"2" => "February",
			"3" => "March",
			"4" => "April",
			"5" => "May",
			"6" => "June",
			"7" => "July",
			"8" => "August",
			"9" => "September",
			"10" => "October",
			"11" => "November",
			"12" => "December",
			);
	$month=$dates[$str];
	return $month;
}


function printArticlesGrid(){
}
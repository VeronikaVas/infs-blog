<?php 
include_once './classes/archive.php';
include_once './classes/article.php';
include_once './classes/category.php';
include_once './classes/user.php';
include_once './views/template-functions.php';

include_once './views/header.php';
?>

	<div class="clearfix"></div>
	<section id="content">
	
			<?php 
            if( isset($_GET["path"]) && file_exists($_GET["path"]))
                include_once( $_GET["path"] );
            else {                    
                include_once( "homepage.php" );	
            }
        	?>  
    </section>
<?php include_once 'views/sidepanel.php'; ?>	
<div class="clearfix"></div>
<?php include_once './views/footer.php';?>
<?php
session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Zend administrace</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../style/main.css">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,900'	rel='stylesheet' type='text/css'>
        <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
        <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    </head>
    <body>
    	<div id="wrapper">
        <header>
           <?php include('views/menu.php');?> 
        </header>
        <main >

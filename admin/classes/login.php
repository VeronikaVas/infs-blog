<?php
include_once 'database.php';

class Login {

	private $db;
	
	function __construct() {
		$this->db = new Database();
	}

	public function login($login, $password){
		//$sha1Password = sha1($password);
		$result = $this->db->select("SELECT * FROM user WHERE username='$login' AND password='$password'");
		
		if($result->num_rows > 0){
			session_start();
			while($row = $result->fetch_assoc()) {

				$_SESSION["username"] = stripcslashes($login);
				$_SESSION["id_user"] = $row["id_user"];
				$_SESSION["privileges_level"] = $row["privileges_level"];
				header("Location: index.php");
				die();
			}
		}else{
			echo "Wrong username or password";
		}
	}

	public function logout() {
		session_start();
		session_destroy();
		header("Location: ../index.php");
	}

}
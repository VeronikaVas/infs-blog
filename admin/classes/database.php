<?php
class Database{

	public $conn;
    public $db_name = "dovy";

    function __construct() {
        $this->conn = mysqli_connect("localhost", "root", "", "zend");
        $this->conn->query("set names utf8");
    }
    
    public function select($sql) {     
        $result = $this->conn->query($sql);
        return $result;      
    }
    
    public function insert($sql) {
        if ($this->conn->query($sql) === TRUE) {
            return TRUE;
        } else {
            echo "Error: " . $sql . "<br>" . $this->conn->error;
        }
        
    }
    
    public function update($sql) {
        return $this->insert($sql);
    }
    
    public function delete($sql) {
        return $this->insert($sql);
    }
}



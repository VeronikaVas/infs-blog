<?php
include_once 'database.php';
include_once 'user.php';

class Article {

	private $db;
	private $user;

	function __construct()
	{
		$this->db = new Database();
		$this->user = new User();
	}

	public function getArticles($id_user) {
		if($this->user->getUserPrivileges($id_user) == 1){
			return $this->getAllArticles();
		}else{
			return $this->getUsersArticles($id_user);
		}
	}

	private function getAllArticles() {
		$result = $this->db->select("SELECT * FROM post");

		if($result->num_rows > 0){
			$return = $this->getTableHeader();
			while($row = $result->fetch_assoc()){
				$id_post = $row["id_post"];
				$name = $row["header"];
				$date = $row["date"];
				$id_category = $row["id_category"];
				$id_user = $row["id_user"];
				$user = $this->user->getUserName($id_user);
				$return .= $this->renderTableBody($name, $user, $date, $id_post, $id_category);
			}
			$return .= "</table>";
		}else{
			return "No articles.";
		}

		return $return;
	}

	private function getUsersArticles($id_user) {

		$sql = "SELECT * FROM post WHERE id_user='$id_user'";
		$result = $this->db->select($sql);

		if(empty($result)){
			return "User didn�t add any post.";
		}

		if($result->num_rows > 0){
			$return = $this->getTableHeader();
			while($row = $result->fetch_assoc()){
				$name = $row["header"];
				$date = $row["date"];
				$id_category = $row["id_category"];
				$id_user = $row["id_user"];
				$user = $this->getUserName($id_user);
				$return .= $this->renderTableBody($name, $user, $date, $id_contents);
			}
			$return .= "</table>";
		}

		return $return;
	}

	private function getTableHeader() {
		$result = "<table>\n";
		$result .= "\t<tr><th>Title</th><th>Author</th><th>Date</th><th>Category</th><th>Action</th></tr>\n";
		return $result;
	}

	private function renderTableBody($name, $author, $date, $id_post, $category) {
		$result = "<tr><td>$name</td><td>$author</td><td>$date</td><td>$category</td><td><a href='edit-article.php?article=$id_post'>Edit</a> <a href='function/delete.php?delete-article=1&id_contents=$id_post'>Delete</a></td></tr>";
		return $result;
	}

	public function getArticle($id_contents) {

		$sql = "SELECT * FROM post WHERE id_post='$id_contents'";

		$result = $this->db->getSelect($sql);

		if($result->num_rows > 0){
			while($row = $result->fetch_assoc()){
				$contents = $row["contents"];
				$name = $row["name"];
				$tags = $this->getTags($id_contents);
				return $this->renderEditForm($name, $tags, $contents, $id_contents);
			}
		}
		return 0;
	}




}


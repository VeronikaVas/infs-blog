<?php
include_once 'database.php';

class User{
	private $db;
	
	function __construct() {
		$this->db = new Database();
	}
	
	
	public function insertUser($nick, $password, $privileges_level)
	{
		//$sha1password = sha1($password);
		$sql = "INSERT INTO user ( username, password, privileges_level) VALUES ('$nick', '$password', '$privileges_level')";
		$result = $this->db->insert($sql);
	
		if($result){return $result;}
		else{return false;}
	}
	
	public function insertUserDescription($id_user, $name, $surname, $nick, $email ,$avatar, $user_bio, $website)
	{
		$sql = "INSERT INTO user_description (id_user, first_name, last_name, email, user_bio, website, avatar_url) VALUES ('$id_user','$name', '$surname', '$email', '$user_bio', '$website', '$avatar')";
		$result = $this->db->insert($sql);
	
		if($result){return $result;}
		else{return false;}
	}
	
	private function getTableHeader() {
		$return = "<table>\n";
		$return .= "\t<tr><th>User</th><th>E-mail</th><th>Actions</th></tr>\n";
		return $return;
	}
	
	private function getTableRow($login, $email, $id_user) {
		$return = "<tr>";
		$return .="<tr><td>$login</td><td>$email</td><td><a href=\"edit-user.php?user=$id_user\">Edit</a> \t <a href=\"function/delete.php?id_users_data=$id_user&delete_user=1\">Delete</a></td></tr>";
		$return .= "</tr>\n";
	
		return $return;
	}
	
	public function getUsersTable() {
		$sql = "SELECT username, id_user FROM user";
	
		$result = $this->db->select($sql);
		$return = $this->getTableHeader();
	
		if($result->num_rows > 0){
			while($row = $result->fetch_assoc()) {
				$id_user = $row["id_user"];
				$login = $row["username"];
	
				$sql_email = "SELECT 'email' FROM user_description WHERE id_user='$id_user'";
				$result_email = $this->db->select($sql_email);
	
				if($result_email->num_rows > 0){
					while($row_email = $result_email->fetch_assoc()) {
	
						$email = $row_email["email"];
	
						$return .= $this->getTableRow($login, $email, $id_user);
					}
				}
				else 
					$return .= $this->getTableRow($login, 'google@google.cz', $id_user);
			}
		}
	
		$return .="</table>";
	
		return $return;
	}
	
	public function editUserFormBody($id_user, $login, $email ) {
	
		$return = "<form action='' method='post'>";
		$return .="\t<label>Username</label><br />";
		$return .="<input type='text' name='username' value='$login'><br />";
		$return .="\t<label>Password</label><br />";
		$return .="<input type='text' name='password' value=''><br />";
		
		$return .="\t<label>Email</label><br />";
		$return .="<input type='text' name='email' value='$email'><br />";
		

		$return .= "</form>\n";
	
		return $return;
	}
	
	public function getUser($id_user){
		$sql = "SELECT * FROM user WHERE id_user=$id_user";
		$user = $this->db->select($sql);
		
		$sql = "SELECT * FROM user_description WHERE id_user=$id_user";
		$user_description = $this->db->select($sql);
		
		if(($user->num_rows > 0)){
			while ($row = $user->fetch_assoc()){
				$login = $row["username"];
			}
		}
		
		if(($user_description->num_rows > 0)){
			while ($row = $user_description->fetch_assoc()){
				$email = $row["email"];
			}
		}
		
		else 
			$email = "";
		
		return $this->editUserFormBody($id_user, $login, $email);
		
	}
	
	public function getUserPrivileges($id){
		$result = $this->db->select("SELECT privileges_level FROM user WHERE id_user=$id");
		
		if($result->num_rows > 0){
			while($row = $result->fetch_assoc()) {
				$level = $row["privileges_level"];
		
				return $level;
			}
		}
	}
	
	public function getUserName($id){
		$result = $this->db->select("SELECT username FROM user WHERE id_user=$id");
	
		if($result->num_rows > 0){
			while($row = $result->fetch_assoc()) {
				$level = $row["username"];
	
				return $level;
			}
		}
	}
}


<?php
include_once 'database.php';
class Category {
	function __construct() {
		$this->db = new Database ();
	}
	public function insertCategory($name, $id_parent) {
		$sql = "INSERT INTO category ( name, id_parent) VALUES ('$name', '$id_parent')";
		$result = $this->db->insert ( $sql );
		
		if ($result) {
			return $result;
		} else {
			return false;
		}
	}
	private function getTableHeader() {
		$return = "<table>\n";
		$return .= "\t<tr><th>Category</th><th>Parent</th><th>Actions</th></tr>\n";
		return $return;
	}
	private function getTableRow($name, $parent, $id_category) {
		$return = "<tr>";
		$return .= "<tr><td>$name</td><td>$parent</td><td><a href=\"edit-category.php?user=$id_category\">Edit</a> \t <a href=\"function/delete.php?id_users_data=$id_category&delete_user=1\">Delete</a></td></tr>";
		$return .= "</tr>\n";
		
		return $return;
	}
	public function getCategoryTable() {
		$sql = "SELECT * FROM category";
		
		$result = $this->db->select ( $sql );
		$return = $this->getTableHeader ();
		
		if ($result->num_rows > 0) {
			while ( $row = $result->fetch_assoc () ) {
				$id_category = $row ["id"];
				$name = $row ["name"];
				$id_parent = $row ["id_parent"];
				
				$sql_parent_name = "SELECT name FROM category WHERE id_parent='$id_category'";
				$result_parent = $this->db->select ( $sql_parent_name );
				
				if ($result_parent->num_rows > 0) {
					while ( $row_parent = $result_parent->fetch_assoc () ) {
						
						$name_parent = $row_parent ["name"];
						$return .= $this->getTableRow ( $name, $name_parent, $id_category );
					}
				} else
					$return .= $this->getTableRow ( $name, '', $id_category );
			}
		}
		
		$return .= "</table>";
		
		return $return;
	}
	
	public function getCategoriesList() {
		$sql = "SELECT * FROM category";
		
		$result = $this->db->select($sql);
		if ($result->num_rows > 0) {
			while ( $row = $result->fetch_assoc () ) {
				$id = $row["id"];
				$name = $row["name"];
				$return .= "<option value=$id>$name</option>";
			}
		}
		return $return;
	}
}

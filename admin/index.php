<?php
session_start();
?>

<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Admin</title>

<link rel="stylesheet" href="../style/main.css">
<link href='https://fonts.googleapis.com/css?family=Roboto:400,700,900'
	rel='stylesheet' type='text/css'>
<script>
  function delpost(id, title)
  {
	  if (confirm("Are you sure you want to delete '" + title + "'"))
	  {
	  	window.location.href = 'index.php?delpost=' + id;
	  }
  }
  </script>
</head>
<body>

	<div id="wrapper">
		<header>
			<?php include('views/menu.php');?>
		</header>
		
		<h2>Welcome to administration section</h2>
		
	</div>

</body>
</html>
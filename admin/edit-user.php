<?php
include_once 'views/header.php';
include_once 'views/menu.php';
include_once 'classes/user.php';
?>

<h2>Edit user</h2>
   <?php
         $users = new User();
         
         if(isset($_GET["user"]))
         {
         	$id = $_GET["user"];
         	echo $users->getUser($id);
         }
    ?>
<?php
include_once 'views/footer.php';
?>
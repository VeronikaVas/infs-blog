<?php
include_once 'views/header.php';
include_once 'views/menu.php';
include_once 'classes/user.php';
include_once 'classes/category.php';
?>

<div id="add-post-form">
    <form action="php/sql/add-post.php" method="post" enctype="multipart/form-data">
        <label>Title</label><br>
        <input type="text" id="post-title" name="post-title" maxlength="100" required><br>
        
        <label>Text</label><br>
        <textarea id="font-text" name="font-text"required></textarea><br>
        
        <label>Date</label><br>      
        <input type="date" id="post-date" name="post-date" required><br>
        
        <label>Category</label><br>
        <select id="post-category" name="post-category" required>
            <?php
               
                $cat = new Category();
                $cat->getCategoriesList();
                ?>
        </select>

        <label>Image</label><br>
        <input type="file" id="post-image" name="post-image" required><br>

        <div class="clearfix"></div>
        <h2>Tags</h2><br>
        <input type="text" id="post-tags" name="post-tags" placeholder="Tag1, Tag2, Tag3"><br>
        
        <input type="submit" id="post-submit" value="Submit post" />
    </form>
</div>


<?php
include_once 'views/footer.php';
?>